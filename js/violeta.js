google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Scratches per Day'],
  ['Black', 9],
  ['White', 2],
  ['Tabby', 12],
  ['Red', 5],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Scratches per day from different coloured cats', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}