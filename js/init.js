(function($){
  $(function(){

    $('.sidenav').sidenav();

  }); // end of document ready
})(jQuery); // end of jQuery name space

// Dropdown menu
$('.dropdown-trigger').dropdown({
  inDuration: 300,
  outDuration: 225,
  constrainWidth: true,
  hover: true,
  gutter: 0,
  belowOrigin: true,
  coverTrigger: false,
  autoTrigger: true,
});

// Sonine navigacija mobiliajam irenginiui  
$(document).ready(function(){
  $('.sidenav').sidenav();
});

// Karusele  
$('.carousel').carousel({ 
  numVisible: 10,
  fullWidth: true
});

// Tabai  
$(document).ready(function(){
  $('.tabs').tabs();
});



