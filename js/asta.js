
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Nervinuosi, kad kodas neveikia', 8],
  ['Šeima', 4],
  ['Maistas', 1],
  ['Miegas', 1],
  ['Dar truputį nervinuosi',2],
  ['Kodas', 8]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Mano diena', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}